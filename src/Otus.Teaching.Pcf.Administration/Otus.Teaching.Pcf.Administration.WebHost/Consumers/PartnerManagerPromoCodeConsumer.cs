﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Services.Interfaces;
using Otus.Teaching.Pcf.Common;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PartnerManagerPromoCodeConsumer : IConsumer<GivePromoCodeToCustomerMessage>
    {
        private readonly IPartnerPromoCodeService _partnerPromoCodeService;
        public PartnerManagerPromoCodeConsumer(IPartnerPromoCodeService partnerPromoCodeService)
        {
            _partnerPromoCodeService = partnerPromoCodeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerMessage> context)
        {
            await _partnerPromoCodeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
        }
    }
}
